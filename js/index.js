window.addEventListener('load', function() {
  var divEstado = document.getElementById("estado");
  if (navigator.onLine)
    {
      divEstado.className="clOnline";
    }
  else
    {
      divEstado.className="clOffline"
    }
});

window.addEventListener('online', ponerOnline);
window.addEventListener('offline', ponerOffline);

function ponerOnline() {
  
  console.log("estoy online");
  var divEstado = document.getElementById("estado");
  divEstado.className="clOnline";
};
function ponerOffline() {
  console.log("estoy offline");
  var divEstado = document.getElementById("estado");
  divEstado.className="clOffline";
  
  
};

function guardarDatosUsuario() {

  
  var obj = { "nombre":document.getElementById("txtNombre").value, 
              "edad":document.getElementById("txtEdad").value, 
              "email":document.getElementById("eMail").value, 
              "dni":document.getElementById("DNI").value,        
              "nacimiento":document.getElementById("Nacimiento").value};
  var myJSON = JSON.stringify(obj);
  localStorage.setItem("datos",myJSON);
  sessionStorage.setItem("datos",myJSON);
}
function recuperarDatosUsuario() {

  var local=localStorage.getItem("datos");
  var sess =sessionStorage.getItem("datos");
  
  alert("local: " + JSON.parse(local).email + "session: " + JSON.parse(sess).email)
}